//import thư viện express js
const express = require ("express");
const path = require("path");
//khởi tạo app
const app = express();

//khai báo cổng chạy 
const port = 9000;


//khai báo GET API trả ra Company Class
app.get("/companies", (request, response) => {

    response.sendFile(path.join(__dirname + "/app/index.html"));
})
//run app on port
app.listen(port, () =>{
    console.log(`App is running on port ${port}`)
})